# Changelog

All notable changes to this project will be documented in this file. The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/) and this project adheres to [Semantic Versioning](http://semver.org).

## [1.0.0](https://gitlab.com/shared-puppet-modules-group/jitsimeet/-/tags/1.0.0) (2020-05-08)

Many thanks to Florian Pritz (@Bluewind0), most of the changes in the new version
are his.

### Added

- Add support for nginx (#2)
- Make the mechanism to configure jvb, meet and jicofo much more flexible
- Support configuring meet's web interface (interface_config.js)

### Fixed

- Fix merging issues with the custom options (#5)
- Greatly improve the default prosody configuration

[Full Changelog](https://gitlab.com/shared-puppet-modules-group/jitsimeet/-/compare/0.2.0...1.0.0)

## [0.2.0](https://gitlab.com/shared-puppet-modules-group/jitsimeet/-/tags/0.2.0) (2020-04-27)

### Fixed

- Jitsi Meet's upstream changed the name of the `jitsi-videobrige` package to
  `jitsi-videobridge2`. Some manual work may be required to purge the old
  package.

[Full Changelog](https://gitlab.com/shared-puppet-modules-group/jitsimeet/-/compare/0.1.0...0.2.0)

## [0.1.0](https://gitlab.com/shared-puppet-modules-group/jitsimeet/-/tags/0.1.0) (2020-03-26)

- Initial release
