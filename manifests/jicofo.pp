# @summary Jicofo subclass.
#
# @param service_name
#   Name of the jicofo service.
#
# @param package_name
#   Name of the jicofo package to be installed.
#
# @param package_ensure
#   State of the jicofo package.
#
class jitsimeet::jicofo (
  String[1] $service_name,
  String[1] $package_name,
  String[1] $package_ensure,
) {
  contain 'jitsimeet::jicofo::install'
  contain 'jitsimeet::jicofo::config'
  contain 'jitsimeet::jicofo::service'

  Class['jitsimeet::jicofo::install']
  -> Class['jitsimeet::jicofo::config']
  -> Class['jitsimeet::jicofo::service']
}
