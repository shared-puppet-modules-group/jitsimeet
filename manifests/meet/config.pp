# @summary Manages the jitsi meet configuration.
#
class jitsimeet::meet::config {
  $_config = {
    hosts                             => {
        domain => $jitsimeet::jitsi_domain,
        focus  => "focus.${jitsimeet::jitsi_domain}",
        muc    => "conference.${jitsimeet::jitsi_domain}",
    },

    bosh                              => "//${jitsimeet::jitsi_domain}/http-bind",
    clientNode                        => 'http://jitsi.org/jitsimeet',
    focusUserJid                      => "focus@auth.${jitsimeet::jitsi_domain}",
    useNicks                          => false,
    testing                           => {
        enableFirefoxSimulcast => false,
        p2pTestMode            => false
    },
    desktopSharingChromeExtId         => null,
    desktopSharingChromeSources       => [ 'screen', 'window', 'tab' ],
    desktopSharingChromeMinExtVersion => '0.1',
    channelLastN                      => -1,
    enableUserRolesBasedOnToken       => false,
    p2p                               => {
        enabled     => true,
        stunServers => [
            { urls => 'stun:stun.l.google.com:19302' },
            { urls => 'stun:stun1.l.google.com:19302' },
            { urls => 'stun:stun2.l.google.com:19302' }
        ],
        preferH264  => true
    },
  }

  file { "/etc/jitsi/meet/${jitsimeet::jitsi_domain}-config.js":
      ensure  => present,
      content => epp('jitsimeet/meet-config.epp', {
        'config'         => deep_merge($_config, $jitsimeet::meet_custom_options),
      }),
      owner   => 'root',
      group   => 'jitsi',
      mode    => '0644',
  }

  $_interface_config = {
    'DEFAULT_BACKGROUND' => '#474747',
    'DISABLE_VIDEO_BACKGROUND' => false,
    'INITIAL_TOOLBAR_TIMEOUT' => 20000,
    'TOOLBAR_TIMEOUT' => 4000,
    'TOOLBAR_ALWAYS_VISIBLE' => false,
    'DEFAULT_REMOTE_DISPLAY_NAME' => 'Fellow Jitster',
    'DEFAULT_LOCAL_DISPLAY_NAME' => 'me',
    'SHOW_JITSI_WATERMARK' => true,
    'JITSI_WATERMARK_LINK' => 'https://jitsi.org',
    'SHOW_WATERMARK_FOR_GUESTS' => true,
    'SHOW_BRAND_WATERMARK' => false,
    'BRAND_WATERMARK_LINK' => '',
    'SHOW_POWERED_BY' => false,
    'SHOW_DEEP_LINKING_IMAGE' => false,
    'GENERATE_ROOMNAMES_ON_WELCOME_PAGE' => true,
    'DISPLAY_WELCOME_PAGE_CONTENT' => true,
    'DISPLAY_WELCOME_PAGE_TOOLBAR_ADDITIONAL_CONTENT' => false,
    'APP_NAME' => 'Jitsi Meet',
    'NATIVE_APP_NAME' => 'Jitsi Meet',
    'PROVIDER_NAME' => 'Jitsi',
    'LANG_DETECTION' => true,
    'INVITATION_POWERED_BY' => true,
    'AUTHENTICATION_ENABLE' => true,
    'TOOLBAR_BUTTONS' => [
        'microphone', 'camera', 'closedcaptions', 'desktop', 'fullscreen',
        'fodeviceselection', 'hangup', 'profile', 'info', 'chat', 'recording',
        'livestreaming', 'etherpad', 'sharedvideo', 'settings', 'raisehand',
        'videoquality', 'filmstrip', 'invite', 'feedback', 'stats', 'shortcuts',
        'tileview', 'videobackgroundblur', 'download', 'help', 'mute-everyone'
    ],
    'SETTINGS_SECTIONS' => [ 'devices', 'language', 'moderator', 'profile', 'calendar' ],
    'VIDEO_LAYOUT_FIT' => 'both',
    'filmStripOnly' => false,
    'VERTICAL_FILMSTRIP' => true,
    'CLOSE_PAGE_GUEST_HINT' => false,
    'SHOW_PROMOTIONAL_CLOSE_PAGE' => false,
    'RANDOM_AVATAR_URL_PREFIX' => false,
    'RANDOM_AVATAR_URL_SUFFIX' => false,
    'FILM_STRIP_MAX_HEIGHT' => 120,
    'ENABLE_FEEDBACK_ANIMATION' => false,
    'DISABLE_FOCUS_INDICATOR' => false,
    'DISABLE_DOMINANT_SPEAKER_INDICATOR' => false,
    'DISABLE_TRANSCRIPTION_SUBTITLES' => false,
    'DISABLE_RINGING' => false,
    'AUDIO_LEVEL_PRIMARY_COLOR' => 'rgba(255,255,255,0.4)',
    'AUDIO_LEVEL_SECONDARY_COLOR' => 'rgba(255,255,255,0.2)',
    'POLICY_LOGO' => undef,
    'LOCAL_THUMBNAIL_RATIO' => 16.0 / 9,
    'REMOTE_THUMBNAIL_RATIO' => 1,
    'LIVE_STREAMING_HELP_LINK' => 'https://jitsi.org/live',
    'MOBILE_APP_PROMO' => true,
    'MAXIMUM_ZOOMING_COEFFICIENT' => 1.3,
    'SUPPORT_URL' => 'https://community.jitsi.org/',
    'CONNECTION_INDICATOR_AUTO_HIDE_ENABLED' => true,
    'CONNECTION_INDICATOR_AUTO_HIDE_TIMEOUT' => 5000,
    'CONNECTION_INDICATOR_DISABLED' => false,
    'VIDEO_QUALITY_LABEL_DISABLED' => false,
    'RECENT_LIST_ENABLED' => true,
    'OPTIMAL_BROWSERS' => [ 'chrome', 'chromium', 'firefox', 'nwjs', 'electron' ],
    'UNSUPPORTED_BROWSERS' => [],
    'AUTO_PIN_LATEST_SCREEN_SHARE' => 'remote-only',
    'DISABLE_PRESENCE_STATUS' => false,
    'DISABLE_JOIN_LEAVE_NOTIFICATIONS' => false,
    'SHOW_CHROME_EXTENSION_BANNER' => false
  }

  file { "/etc/jitsi/meet/${jitsimeet::jitsi_domain}-interface_config.js":
      ensure  => present,
      content => epp('jitsimeet/interface_config.epp', {
        'config' => deep_merge($_interface_config, $jitsimeet::meet_interface_options),
      }),
      owner   => 'root',
      group   => 'jitsi',
      mode    => '0644',
  }

}
