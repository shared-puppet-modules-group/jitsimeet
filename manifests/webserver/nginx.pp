# @summary Manage the nginx virtualhost.
#
# Original source taken from https://github.com/b1-systems/puppet-jitsi/blob/a3a2b339549ac3398b782d8217bf335744be6f49/manifests/init.pp#L211
#
class jitsimeet::webserver::nginx {
  class { 'nginx':
    service_manage => 'webserver' in $jitsimeet::managed_services,
    manage_repo    => $jitsimeet::nginx_manage_repo,
  }
  nginx::resource::server { "${jitsimeet::jitsi_domain} port 80":
    ensure              =>  present,
    www_root            =>  $jitsimeet::www_root,
    location_cfg_append => { 'rewrite' => '^ https://$server_name$request_uri? permanent' },
  }
  nginx::resource::server { $jitsimeet::jitsi_domain:
    index_files          => [ 'index.html' ],
    listen_port          => 443,
    ssl                  => true,
    ssl_cert             => $jitsimeet::jitsi_vhost_ssl_cert,
    ssl_key              => $jitsimeet::jitsi_vhost_ssl_key,
    use_default_location => false,
    www_root             => $jitsimeet::www_root,
  }

  Nginx::Resource::Location {
    server   => $jitsimeet::jitsi_domain,
    ssl      => true,
    ssl_only => true,
    require  => Nginx::Resource::Server[$jitsimeet::jitsi_domain],
  }

  nginx::resource::location { "${jitsimeet::jitsi_domain} config.js":
    location       => '/config.js',
    location_alias => "/etc/jitsi/meet/${jitsimeet::jitsi_domain}-config.js",
  }

  nginx::resource::location { "${jitsimeet::jitsi_domain} interface_config.js":
    location       => '/interface_config.js',
    location_alias => "/etc/jitsi/meet/${jitsimeet::jitsi_domain}-interface_config.js",
  }

  nginx::resource::location { "${jitsimeet::jitsi_domain} external_api.js":
    location       => '/external_api.js',
    location_alias => '/usr/share/jitsi-meet/libs/external_api.min.js',
  }

  nginx::resource::location { "${jitsimeet::jitsi_domain} default":
    location            => '/',
    location_cfg_append => {
      ssi => 'on',
    },
  }

  nginx::resource::location { "${jitsimeet::jitsi_domain} rewrite":
    location      => ' ~ ^/([a-zA-Z0-9=\?]+)$',
    rewrite_rules => [
      '^/(.*)$ / break',
    ],
  }

  nginx::resource::location { "${jitsimeet::jitsi_domain} bosh":
    location         => '/http-bind',
    proxy            => 'http://localhost:5280/http-bind',
    proxy_set_header => [
      'X-Forwarded-For $remote_addr',
      'Host $http_host',
    ],
  }
}
