# @summary Webserver subclass.
#
class jitsimeet::webserver {
  case $jitsimeet::webserver {
    'nginx': {
      contain 'jitsimeet::webserver::nginx'
    }

    default: {
      fail('That webserver is not supported yet.')
    }
  }
}
