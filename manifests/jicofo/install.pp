# @summary Manages the jicofo package.
#
class jitsimeet::jicofo::install {
  package { $jitsimeet::jicofo::package_name:
    ensure       => $jitsimeet::jicofo::package_ensure,
    responsefile => '/var/local/jitsimeet.preseed',
  }
}
