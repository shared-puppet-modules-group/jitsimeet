# @summary Manages the jicofo configuration.
#
class jitsimeet::jicofo::config {

  file { '/etc/jitsi/jicofo/config':
      ensure  => present,
      content => epp('jitsimeet/jicofo-config.epp', {
        'jitsi_domain'        => $jitsimeet::jitsi_domain,
        'focus_secret'        => $jitsimeet::focus_secret,
        'focus_user_password' => $jitsimeet::focus_user_password,
      }),
      owner   => 'jicofo',
      group   => 'jitsi',
      mode    => '0640',
      notify  => Service[$jitsimeet::jicofo::service_name],
  }

  $_properties = {} + $jitsimeet::jicofo_additional_properties

  $_properties_lines = $_properties.map |$key, $value| {
    "${key}=${value}"
  }

  file { '/etc/jitsi/jicofo/sip-communicator.properties':
    content => $_properties_lines.join("\n")
  }

}
