# @summary Meet subclass.
#
# @param package_name
#   Name of the jitsi meet package to be installed.
#
# @param package_ensure
#   State of the jitsi meet package.
#
class jitsimeet::meet (
  Array[String[1]] $package_name,
  String[1]        $package_ensure,
) {
  contain 'jitsimeet::meet::install'
  contain 'jitsimeet::meet::config'

  Class['jitsimeet::meet::install']
  -> Class['jitsimeet::meet::config']
}
