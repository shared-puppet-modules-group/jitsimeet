# @summary Manages prosody configuration.
#
class jitsimeet::prosody {

  class { 'prosody':
    user                   => 'prosody',
    group                  => 'prosody',
    admins                 => [ "focus@auth.${jitsimeet::jitsi_domain}", ],
    ssl_custom_config      => false,
    c2s_require_encryption => false,
    s2s_require_encryption => false,
    s2s_secure_auth        => false,
    log_sinks              => [],
    custom_options         => {
      'certificates'       => 'certs',
      consider_bosh_secure =>  true,
    },
    components             => {
      "conference.${jitsimeet::jitsi_domain}"        => {
        'type'    =>'muc',
        'options' => {
          'storage' => '"memory"',
        },
      },
      "jitsi-videobridge.${jitsimeet::jitsi_domain}" => {
        'secret' =>  $jitsimeet::jvb_secret,
      },
      "focus.${jitsimeet::jitsi_domain}"             => {
        'secret' =>  $jitsimeet::focus_secret,
      },
    }
  }

  $_jitsi_vhost_options = {
      ensure         => present,
      custom_options => {
        'authentication'         => $jitsimeet::prosody_authentication_method,
        'c2s_require_encryption' => false,
        'modules_enabled'        => [ 'bosh', 'pubsub', 'ping' ],
      },
  }

  $_jitsi_vhost_ssl = $jitsimeet::jitsi_vhost_ssl_key ? {
    default => {
      ssl_key        => $jitsimeet::jitsi_vhost_ssl_key,
      ssl_cert       => $jitsimeet::jitsi_vhost_ssl_cert,
    },
    undef   =>  {},
  }

  prosody::virtualhost {
    $jitsimeet::jitsi_domain:
      * => $_jitsi_vhost_options + $_jitsi_vhost_ssl + $jitsimeet::jitsi_vhost_additional_options,
  }


  $_auth_vhost_options = {
      ensure         => present,
      custom_options => {
        'authentication' => 'internal_plain',
      },
  }

  $_auth_vhost_ssl = $jitsimeet::auth_vhost_ssl_key ? {
    default => {
      ssl_key        => $jitsimeet::auth_vhost_ssl_key,
      ssl_cert       => $jitsimeet::auth_vhost_ssl_cert,
    },
    undef   =>  {},
  }

  prosody::virtualhost {
    "auth.${jitsimeet::jitsi_domain}":
      * => $_auth_vhost_options + $_auth_vhost_ssl,
  }

  prosody::user { 'focus':
    host => "auth.${jitsimeet::jitsi_domain}",
    pass => $jitsimeet::focus_user_password;
  }

  prosody::user { 'jvb':
    host => "auth.${jitsimeet::jitsi_domain}",
    pass => $jitsimeet::jvb_secret;
  }

  exec {
    'update-ca-certificates':
      command     => '/usr/sbin/update-ca-certificates -f',
      refreshonly => true;
  }

  file {
    default:
      ensure => link,
      force  => true,
      notify => Exec['update-ca-certificates'];
    "/usr/local/share/ca-certificates/auth.${jitsimeet::jitsi_domain}.key":
      target => "/etc/prosody/certs/auth.${jitsimeet::jitsi_domain}.key";
    "/usr/local/share/ca-certificates/auth.${jitsimeet::jitsi_domain}.crt":
      target => "/etc/prosody/certs/auth.${jitsimeet::jitsi_domain}.crt";
  }
}
